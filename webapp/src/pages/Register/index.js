import React, { useState } from "react";

import userProvider from "../../providers/user";

import "./style.css";

const RegisterComponent = props => {
  const [email, setEmail] = useState("");
  const [pass, setPass] = useState("");
  const [repass, setRepass] = useState("");

  const changeEmail = event => setEmail(event.target.value);

  const changePass = event => setPass(event.target.value);

  const changeRepass = event => setRepass(event.target.value);

  const register = async event => {
    event.preventDefault();

    if (pass === repass) {
      const response = await userProvider.register(email, pass);
      if (response.data) {
        props.history.push("/");
      }
    }
  };

  return (
    <div className="container max-height">
      <div className="col col-center">
        <img
          src="http://luciodesigner.com.br/wp-content/uploads/2017/07/tamanho-logo-300x143.png"
          alt="Logo"
        />
      </div>
      <div className="col col-center">
        <form onSubmit={register}>
          <input placeholder="e-mail" onChange={changeEmail} />
          <input
            placeholder="senha pessoal"
            type="password"
            onChange={changePass}
          />
          <input
            placeholder="senha novamente"
            type="password"
            onChange={changeRepass}
          />
          <button tyoe="submit">Cadastrar</button>
          <a href={"/"}>ir para o login</a>
        </form>
      </div>
    </div>
  );
};

export default RegisterComponent;
