import React, { useState } from "react";
import { useSelector } from "react-redux";
import DateTimePicker from "react-datetime-picker";
import Geosuggest from "react-geosuggest";

import userProvider from "../../../providers/user";

import "./style.css";

const ConfigDashboardComponent = props => {
  const [name, setName] = useState("");
  const [photo, setPhoto] = useState("");
  const [bornIn, setBornIn] = useState({});
  const [bornAt, setBornAt] = useState(new Date());

  const user = useSelector(state => state.user);

  const handleUpdateProfile = async event => {
    event.preventDefault();

    const response = await userProvider.update(
      user._id,
      name,
      "image.jpg",
      bornIn,
      bornAt
    );

    if (response.data) {
      props.history.push("/dashboard");
    }
  };

  return (
    <div className="container">
      <form onSubmit={handleUpdateProfile}>
        <input placeholder="Nome" onChange={e => setName(e.target.value)} />
        <input
          type="file"
          placeholder="Foto"
          onChange={e => setPhoto(e.target.value)}
        />
        <Geosuggest
          placeholder="local de nascimento"
          onSuggestSelect={suggest => setBornIn(suggest.location)}
        />
        <DateTimePicker value={bornAt} onChange={date => setBornAt(date)} />
        <button type="submit">Salvar</button>
      </form>
    </div>
  );
};

export default ConfigDashboardComponent;
