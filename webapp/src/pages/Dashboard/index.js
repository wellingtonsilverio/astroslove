import React from "react";
import { Route } from "react-router-dom";
import { useDispatch } from "react-redux";

import ConfigDashboardComponent from "../../pages/Dashboard/Config";
import ChatDashboardComponent from "../../pages/Dashboard/Chat";
import MatchDashboardComponent from "../../pages/Dashboard/Match";

import userProvider from "../../providers/user";

import "./style.css";

const DashboardComponent = props => {
  const dispatch = useDispatch();

  const _id = localStorage.getItem("userId");
  if (!_id) {
    props.history.push("/");
  } else if (props.location.pathname !== "/dashboard/config") {
    const checkUserCompleteProfile = async () => {
      const response = await userProvider.findById(_id);

      if (response.data) {
        dispatch({ type: "UPDATE_ID", _id: response.data._id });
        const { name, photo, bornIn, bornAt } = response.data;
        if (!name || !photo || !bornIn || !bornAt) {
          props.history.push("/dashboard/config");
        }
      }
    };
    checkUserCompleteProfile();
  } else {
    dispatch({ type: "UPDATE_ID", _id });
  }

  const handleLogoff = () => {
    localStorage.removeItem("userId");
    props.history.push("/");
  };

  return (
    <div className="container">
      <h1>Astroslove</h1>
      <button onClick={handleLogoff} className="exit-button">
        Sair
      </button>
      <Route path="/dashboard/config" component={ConfigDashboardComponent} />
      <Route path="/dashboard/chat" component={ChatDashboardComponent} />
      <Route path="/dashboard/match" component={MatchDashboardComponent} />
    </div>
  );
};

export default DashboardComponent;
