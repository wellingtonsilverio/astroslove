import provider from "../index";

export default {
  login: (email, pass) => {
    return provider.post("/login", {
      email,
      pass
    });
  },
  register: (email, password) => {
    return provider.post("/users", {
      email,
      password
    });
  },
  findById: _user => {
    return provider.get(`/users/${_user}`);
  },
  update: (_id, name, photo, bornIn, bornAt) => {
    return provider.put(`/users/${_id}`, {
      name,
      photo,
      bornIn,
      bornAt
    });
  }
};
