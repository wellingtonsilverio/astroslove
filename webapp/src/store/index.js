import { createStore, combineReducers } from "redux";

import UserReducer from "./user";

export default createStore(
  combineReducers({
    user: UserReducer
  }),
  []
);
