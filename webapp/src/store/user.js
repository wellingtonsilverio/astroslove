const INITIAL_STATE = {
  _id: ""
};

export default function user(state = INITIAL_STATE, action) {
  switch (action.type) {
    case "UPDATE_ID":
      return { ...state, _id: action._id };
    default:
      return state;
  }
}
