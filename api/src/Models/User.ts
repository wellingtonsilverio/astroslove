import { model, Document, Schema, Types } from "mongoose";

export interface IUser extends Document {
  email: string;
  password: string;
  code: number;
  name: string;
  photo: string;
  bornAt: Date;
  bornIn: {
    lat: number;
    lng: number;
  };
  location: {
    coordinates: Array<number>;
    type: string;
  };
  birthChart: {
    sign: string;
    resingSgn: string;
    houses: Array<string>;
    planets: Array<string>;
  };
  matchs: Array<{
    _user: Types.ObjectId;
    createdAt: Date;
  }>;
  active: boolean;
}

const UserSchema: Schema = new Schema(
  {
    email: { type: String, required: true, unique: true },
    password: { type: String, required: true },
    code: { type: Number },
    name: { type: String },
    photo: { type: String },
    bornAt: { type: Date },
    bornIn: {
      lat: Number,
      lng: Number
    },
    location: {
      coordinates: { type: [Number], required: true },
      type: { type: String, default: "Point" }
    },
    birthChart: {
      sign: { type: String },
      resingSgn: { type: String },
      houses: { type: [String] },
      planets: { type: [String] }
    },
    matchs: [
      {
        _user: { type: Types.ObjectId, ref: 'User' },
        createdAt: { type: Date }
      }
    ],
    active: { type: Boolean, default: true }
  },
  { timestamps: true }
);

export default model<IUser>("User", UserSchema);
