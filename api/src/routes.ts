import * as express from "express";

import { UserController } from "./Controllers/User";
import { ChatController } from "./Controllers/Chat";
import { MatchController } from "./Controllers/Match";
import { LoginController } from "./Controllers/Login";

export const router = express.Router();

// Users
router.get("/users", UserController.index);
router.get("/users/:_id", UserController.show);
router.post("/users", UserController.store);
router.put("/users/:_id", UserController.update);
router.put("/users/:_id/active", UserController.restore);
router.put("/users/:_id/delete", UserController.destroy);

// Chats
router.get("/chats", ChatController.index);
router.get("/chats/:_id", ChatController.show);
router.post("/chats", ChatController.store);
router.put("/chats", ChatController.update);
router.put("/chats/:_id/active", ChatController.restore);
router.put("/chats/:_id/delete", ChatController.destroy);

// Match
router.post("/matchs", MatchController.store);

// Login
router.post("/login", LoginController.show);
