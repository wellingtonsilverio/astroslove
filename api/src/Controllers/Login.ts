import UserModel from "../Models/User";

export const LoginController = {
  index: async (req, res) => {},
  store: async (req, res) => {},
  show: async (req, res) => {
    try {
      const user = await UserModel.findOne({
        email: req.body.email,
        password: req.body.pass
      });

      res.status(200).json(user);
    } catch (error) {
      res.status(400).json(error);
    }
  },
  update: async (req, res) => {},
  destroy: async (req, res) => {},
  restore: async (req, res) => {}
};
