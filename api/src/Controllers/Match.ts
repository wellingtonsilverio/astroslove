import UserModel, { IUser } from "../Models/User";
import ChatModel from "../Models/Chat";
import * as moment from "moment";

const scoreByDistance = (distance: number) => {
  return 0.001 * distance;
};
const scoreBySynastry = async (user: IUser) => {
  return 0;
};

export const MatchController = {
  index: async (req, res) => {},
  store: async (req, res) => {
    try {
      const user = await UserModel.findById(req.body._id);

      if (user.matchs.length > 0 && moment().add(-1, 'day').diff(moment(user.matchs[user.matchs.length - 1].createdAt)) < 0)
        return res.status(200).json({ userAlreadyAnMatchToday: true });

      // Busca os 10 users mais próximos que não estao em user.matchs
      const matchs = await UserModel.aggregate([
        {
          $geoNear: {
            near: { type: "Point", coordinates: user.location.coordinates },
            spherical: true,
            distanceField: "distance"
          }
        },
        {
          $match: {
            $and: [
              { _id: { $ne: user._id } },
              { _id: { $nin: user.matchs.map(match => match._user) } }
            ],
            active: true
          }
        },
        { $limit: 100 }
      ]);

      if (matchs.length < 1)
        return res.status(200).json({ noUsersProvidedForMatch: true });

      // acessa a API e faz o score
      // Escolhe o que tem maior score
      let bestUser: any = { // TODO
        score: -10000
      };

      for (const _user of matchs) {
        const score = await scoreBySynastry(_user) - scoreByDistance(_user.distance);

        if (score > bestUser.score) {
          bestUser = _user;
          bestUser.score = score;
        }
      }

      // Adiciona o user._id em user.matchs
      user.matchs.push({
        _user: bestUser._id,
        createdAt: moment().toDate()
      });

      await user.save();

      // Cria o chat
      const createChat = await ChatModel.create({
        _users: [user._id, bestUser._id]
      });

      const chat = await ChatModel.findById(createChat._id).populate('_users');

      res.status(201).json({ chat, bestUser });
    } catch (error) {
      res.status(400).json(error);
    }
  },
  show: async (req, res) => {},
  update: async (req, res) => {},
  destroy: async (req, res) => {},
  restore: async (req, res) => {}
};
